#![allow(non_camel_case_types)]

#[macro_use]
extern crate structopt;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "cult")]
struct Cult {
    _unused: String,
    #[structopt(subcommand)]
    cmd: Command
}

#[derive(StructOpt, Debug)]
enum Command {
    science,
    programming,
    autotools,
}

fn main() {
    let opt = Cult::from_args();
    match opt.cmd {
       Command::science => println!("Mr Feynman is watching you... 👀"),
       Command::programming => println!("Seriously? Again from stack overflow?"),
       Command::autotools => println!("My thoughts and prayers are with you 😢"),
    }
}
