# Cargo Cult

A cargo utility that makes fun of [Cargo Cults][cults]

If you are wondering why this thing exists I suggest reading [this small][toot]
toot thread on Mastodon.

## Usage

```sh
➜  ~ cargo install cargo-cultcmd
➜  ~ cargo cult science
Mr Feynman is watching you... 👀
➜  ~ cargo cult programming
Seriously? Again from stack overflow?
```

Many thanks to [Federico][federico] and [Paul][paul].

[cults]: https://en.wikipedia.org/wiki/Cargo_cult
[paul]: https://ceilidh.space/@balrogboogie
[federico]: https://people.gnome.org/~federico/#about
[toot]: https://mastodon.social/@federicomena/99847817725800571